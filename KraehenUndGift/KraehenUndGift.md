Die Krähen sind so wie sie sind, weil sie im Herbst den Kohl von den
Bauern auf dem Feld fressen. Die Katzen hingegen sind so, weil sie den
Kohl nicht fressen und auch die Krähen nicht erwischen. Der Bauer
pflanzt den Kohl, damit die Krähen bleiben und Raupen und Schnecken
auf den Feldern weg essen. Weil die Krähen das nicht wissen, essen sie
doch den Kohl und nicht nur Schnecken. Deswegen gibt es immer wieder
in den Medien Diskussionen über Bauern, Krähen und Unkrautgifte von
Bayer. Früher gab es Bayer 05 Uerdingen. Die haben auf Asche Fussball
gespielt aber weil die aus Kohle war und die alle ging, gibt es
auch Uerdingen nicht mehr. Deswegen macht Bayer nun Gift für grüne
Flächen von Bauern. Da kann man dann auch drauf Fußballspielen.
Fußball ist sehr wichtig in Deutschland und Krähen nun mal nicht so.
Das, sagt mein Papa, ist Kultur. Und Kultur brauchen wir mehr als Krähen.
Ende.


*ausreichend (4+), Frau Vogel*

