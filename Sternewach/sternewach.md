da liegst du so im bett und kannst nicht schlafen. du starrst aus
dem fenster. hey, hat der stern mir gerade zugeblinzelt?
seltsamer lichtschein. sterne sehn aus, als sieht man von weit oben
auf eine gruppe von leuten mit taschenlampen, die ihren
lichtkegel auf die erde richten. passt man nicht auf oder schaut nicht
hin, spielen die mit den lichtkegeln und versuchen sich gegenseitig
zu blenden. Da! Schon wieder! hab nicht aufgepasst.

ein paar, die etwas zu spät kamen, huschen hin und her, schlagen mit dem
handballen seitlich auf die taschenlampe und sagen: Verdammt! Die
Batterie is locker!

die nacht geht vorbei. erste busse hört man. du bist immer noch wach. 
siehst im dunst die sonne aufgehen und es erinnert daran, als würde
man hinter einer rauchwolke die glut eines zigarrenstummels sehn.
da schaut wer flach übern tisch im tabakdunst und denkt sich:
näh, puh, wat hammse da schon wieder gebaut? bah!

dabei fällt ihm der stummel in den kaffee und auf der sonnenseite
der erde: Tsunami. Der Radiowecker geht. Scheiss nachrichten. die nacht
und die ruhe ist vorbei. nach 4 Tagen ohne Kaffee sind wohl 4 Kaffee an
einem Tag keine gute dosis.