dawid bowie tod. die punkte nachts an der wand
an der holzwand hinterm schreibtisch fallen wie
tote spinnen ab. schau hin und seh, es ist nur in
deinem kopf. der flur zur wohnung, das treppenhaus
du stellst dir alles vor wie van gochs nachtkaffe und
die sterne kreisen übers feld hell erleuchtet. Deine
leute sagen dir tu das mach das das gibt dir
den wisch für den leben für deine rente dein geld
deine sicherheit auf einsamkeit im alter so sei es.
die struktur deines lebens eine liternei der oberen
ein scheitern oder mal die luft raus das geht nicht. nicht
urlauben können weil phanatsielose tage geplant nur wenig
mit freiheit einhand zu gehen scheinen. Dexter im Tv ödet
dich an. auf jeder freifläche deiner wohnung ein offenes
büchermaul schreit dir zum kopfe "lerne mich, lerne, lerne".
alles um dich rum frisst deine Zeit und schlägt dir
vor wo du nichts von hälts, nicht hinter stehst. einfach mal
handy zerschmettern können, abbauen ohne die stigmata der
umliegenden geordneten. verrückt sind die unfreien die
sich dem schema der normalität fügen. Dada lebt in deinem
"es" was auch malt. Jedes bild ein maschhaufen mist ohne
strucktur ein wahnbild meines inneren. last mich in ruhe. Last.
stille selbst stille durch summendes blutes in ohren höre
ich die heizung, kühlschrank seuselnde akkugase und blasen des
macbook ein haufen radau. nicht klar denken. nichts denken.
stumpfsinn als einzigen ausweg zum zwang des masters, des
bachlors. stumpfsinn, gleichmut und langeweile im einklang
mit der Wand, dem Tisch, dem Stuhl. Nichts essen müssen, nichts
meinen Eltern zeigen, sagen, erklären müssen. starren. dasitzen
und starren. jeder vorschlag um anderen in den kram zu passen.
leck mich. ein paar in die fresse rein. weltuntergang erwartend in
vorfreude auf das wesentliche sich konzentrieren zu müssen.
keine plugins für schneller, keine software für hardware, kein
bling bling und bam bam und kein düdellü. Wir schön so ein
smartphohne von zweck befreit, zubeult und aufgeplatzt sein kann.
Das Ende kann in öl getaucht werden ohne in feuer mann blau
rot gemeint auf wiedersehn. Denken einfach mal eingestellt.
Xilinx, MPI, MPE, Thread und Mandelbrot in Asynchroner Multi
Prosoning am Arsch mich das alles kann, wahlweise auch
zum mitnehmen, ihr verschimmelten Kaffeebohnenröster, wie ihr
alle hier seid!