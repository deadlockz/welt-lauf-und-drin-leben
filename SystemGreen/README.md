\begin{center}
\begin{Large}System Green ist Menschenfleisch\end{Large} \\
\emph{von Jochen Peters (August 2018)}

\includegraphics[width=10cm]{title.png}
\end{center}

## Woraus wir sind

,,Keine Mutanten. Keine Roboter'', sagte Frank seiner M. Leider hatte
Franks Schulprojekt in der 4. Klasse beides, wodurch er sich
disqualifizierte. ,,Mein Lehrerix wollte sich auf keine Diskussion
einlassen, dass es mutierte Roboterameisen sind. Es sind also nicht bloß
Mutanten oder Roboter'', erläuterte Frank. Das hatte nun Eliza davon,
dass sie ihr K auf ein rückständige Schule schickte, wofür sie 3 Jahre
nach Franks Geburt per Gesetz gekämpft hatte. Der größte Teil der M und
Ps übergab heute nach der Geburt ihr K dem staatlichen Bildungssystem.
Die gesamte Schullaufbahn wird dort mit VR-Brillen simuliert -- in der
Hälfte der Zeit. Dies war aber nur durch eine komplette Einbindung des
Organismus möglich, um Ruhezeiten an die Individuen an zu passen. Eine
Störung durch andere Menschen ist in der Zeit verboten.

Als Eliza in dieses Bildungswesen kam, wurden es durch Firmenspenden
finanziert. Nach dem großen Copyright-Crash 2025 hatte der Staat über
Jahrzehnte keine Gelder mehr für Bildung übrig und das Bildungswesen
wurde zunehmend privatisiert. Leider war damals ein Werbefilm in die
VR-Brillen eingebaut, was man erst nach Jahren heraus fand. Eliza hatte
ein ,,Telepig'' Trauma erlitten und wollte solche Patzer ihrem K ersparen.

Sie war mit Frank gerade auf dem Weg zu Doktorix. Franks P arbeitete
noch. Die Firma Zeta empfahl ihr eine weitere Schwangerschaft, da ihre
Eier nicht den Standards entsprachen, sie für lange Zeit bis nach der
Erwerbstätigkeit einfrieren zu lassen. Zum Glück konnte man dies nun
feststellen, nachdem die Firma Beta Anfang 2031 eine massive Panne beim
Einfrieren der Eizellen ihrer Mitarbeiterinnen einräumen musste. Die
Zellen konnten nur noch der Forschung übergeben werden, woraus später
die ersten biologischen, neuralen Prozessoren entstanden. Jedes System,
das aus diesen natürlichen Komponenten bestand, wurden seitdem ,,System
Green'' genannt.

Der Weg zu Doktorix war vergleichsweise weit. Normalerweise war
Wohnsitz, Schule, Arbeitsplatz nicht mehr als 20 Minuten von einander
entfernt, aber das seit Jahren eingeführte Wohnung-to-go und
Arbeit-to-go Konzept war nicht ganz unproblematisch. Es wurden zwar
Wohnungen und Arbeitsplätze überall zur Verfügung gestellt und dank
3D-Druckern, Recycling und dem guten altem Industrie 4.0 Standard alles
in Minutenschnelle in deine Wohnung und Arbeitsplatz umgebaut, jedoch
waren durch die Natur manche Orte von Menschen bevorzugt. Die
westfälische Küste war wegen des kühlen Windes am Abend zum Wohnen
populärer als zum Arbeiten. Trotz Vereinheitlichung war somit trotzdem nur
schwer dort ein ,,Wohn'' zu bekommen.

Doktorix Schneider machte beim Arbeit-to-go Programm der Regierung nicht
mit und hatte seine Praxis nahe der Küste. Bestimmtes Arbeitsmaterial
war nicht druckbar und der Transport seie zu aufwändig, so 
Schneider. Umzugs-Unternehmen verlangten enorme
Summen und versprachen einem innerhalb eines Tages den Umzug durch zu
führen und am Abend ein fertiges Abendessen von Robotern zubereitet auf
dem Tisch vorfinden zu können. Nach der Entschädigung der Regierung für
das ,,Telepig'' Trauma hatte Eliza einmal in Düsseldorf ein
Umzugs-Unternehmen beauftragt. Am Abend fand sie ihre Kostbarkeiten
schlampig imitiert in der neuen Wohnung wieder und, statt eines
dampfenden Abendessens, einen 24h gültigen Nahrungsmittel-Gutschein für
,,Mac Goos Nr 32'' -- ein Nahrungsmittel-Lieferant, der in Düsseldorf,
aber nicht an der westfälischen Küste aktiv war. Außerdem war als
Umzugsziel die unmittelbare Küste ausgemacht und nicht 2km davon entfernt.
Ihr Versuch, das Unternehmen zu verklagen, kostete sie Nerven und Geld,
da die Firmenleitung ausschließlich aus künstlichen Geschäftsorganen
bestand, welche nicht als juristische Person anerkannt wurde. Die Person,
die diese Maschinen bewachte und den größten Gehaltscheck bekam, konnte
dafür nicht zur Verantwortung gezogen werden. Juristisch wären
die Maschinen für den Fehler beim Umzug verantwortlich gewesen -- jedoch waren
diese nicht verklagbar. Ein Elektonengehirn und einen Algorithmus hatte
man bisher noch nie in eine Gefängniszelle gesteckt. Die moderne
elektronische Datenverarbeitung hatte es seit Jahren erfolgreich geschafft,
wirtschaftliche Macht inklusive juristisch- und moralische Freiheit zu erlangen.

Frank und Eliza saßen nun bei Doktorix Schneider im
Vorraum mit zwei weiteren Menschen, die Hochschwanger aus sahen. Der Rechte war mit seinem Bart
medikamentös etwas fragwürdig eingestellt; auch Frank wunderte sich
über diesen Herrn. Er schaute seine M etwas irritiert an: ,,Schneider ist
schon seit 30 Jahren in dem Geschäft, Frank, und weiß, wie
der Hase läuft.'' Nach wenigen Minuten war Eliza dran. Schneider wirkte
beim Herein-Winken ins Büro ein wenig mechanisch.

,,Ich muss ihnen leider sagen, dass ihr K das BBT haben wird.'' Eliza
hatte es schon geahnt. In den letzten Jahren trat BBT -- die Blob
Buildung Transcription -- immer häufiger auf. Diese K kamen als eine Art
Pudding zur Welt und nur wenige Fälle von hohen Industriellen waren
bekannt, wo diese Menschen in einer Art Rollstuhl ein Leben führen
konnten. Durch Umweltgifte war laut Industry-News-Networks das Genmaterial der Menschen
stark verändert worden. Elizas P hatte vor seinem Tod noch die
Vermutung, dass diese Gifte erst durch die Menschen in die Umwelt kamen.
Diese Sichtweise war aber inzwischen von der Industrie als üble
Verleugnung und in ein Gesetz gegen die Aussagen von systemrelavanten
Versorgungsinfra\ldots

,,Wollen Sie es spenden?'', unterbrach Schneider die in Gedanken versunkene M.

,,Spenden? Ich dachte eher sie erklären mir, warum trotz Gentherapie
nicht mein Wunschkind hier im Gespräch ist?''

Schneider darauf: ,,Das Genmaterial wird knapp. Durch die Therapie sind
bestimmte Teile bei M und Ps populär geworden, die sich nun in der
Nachkommenschaft zu sehr ähneln. Ich wusste nicht, dass die
Wunscheigenschaften, die sie wollten, bereits bei Ihnen zum Einsatz
kamen. Ihre Eltern hatten damals für sie nur eine 1-Generationen Lizenz
auf hohe Intelligenz.'' Frank schaut seine M fragend an: ,,Bin ich dann
dumm oder werde ich noch flüssig?'', und bricht danach in Tränen aus.
,,Sehen Sie, was sie getan haben? Jetzt weiß er, dass er von mir höhere
Intelligenz nicht bekommen hat.'' Schneider versucht zu beruhigen: ,,Sie
können ja eine 2-Generationen Lizenz erwerben statt der 1-Gen Lizenz.''

Eliza: ,,Aber die kann sich doch kaum wer leisten und meinem Ungeborenen
nützt das nichts mehr.''

Schneider: ,,Der Staat reguliert mit den Lizenzen, dass es eine
ausgeglichene gesellschaftliche Struktur gibt. Da kann ich leider nichts
machen.''

Eliza schluchzte nun genauso auf ihrem Stuhl vor sich hin, wie ihr Sohn Frank.

Schneider: ,,Was ihren BBT Embryo angeht, da kenne ich einen weltweiten
Händler, der daran Interesse hätte. Ich denke, der könnte uns im
Gegenzug eine 2-Gen Lizenz aus einem anderen Land besorgen, wo
Intelligenz staatlich durch andere Mittel reguliert wird. Das habe ich
schon ein paar mal gemacht.''

Eliza beruhigte sich etwas. Sie hatte davon auch schon gehört. Teile von
Beta und deren produzierten Chips wurden günstig im Ausland hergestellt.
Man munkelte, dass seit Jahren eigentlich alle Eizellen von 2031 schon
längst hätten aufgebraucht sein müssen, aber offenbar gab es für diesen
Markt eine neue Quelle. Sie konnte sich aber auch nicht entsinnen, dass
ihre Eltern jemals von Begriffen wie 1-Gen oder 2-Gen Lizenzen
gesprochen hätten, als sie ihren Bruder planten. Vielleicht hatte sie
dies ja nur durch das ,,Telepig'' Trauma vergessen und sie wurde blind
für diese Begriffe, nachdem sie aus dem Bildungswesen an M und P
übergeben wurde. Dass durch die Bildung mit VR-Brillen eine solche
Blindheit möglich sein kann, wurde ihr durch das Trauma klar: In dem
damals eingebauten Werbefilm wurde ein Zeichentrick Schweinchen ganz
langsam zu einem Menschen und dann wieder zu einem Schwein. Kombiniert
mit dem jungen Alter der Kinder und anderen Inhalten des Bildungswesens,
konnte sie lange Schwein und Mensch auf Bildern nicht unterscheiden.
Auch andere, ständig wiederholte Begriffe wurden für sie hohl,
unsichtbar, bedeutungslos. Man könnte fast sagen, dass\ldots

Schneider unterbrach: ,,Was ist denn nun?''

Eliza: ,,Ok, Frau Schneider, so machen wir es.''

## Sonne

Es wurde Abend, als sie mit Frank die Praxis verließ. Das blau-grüne
Licht der Sonne über der Nordsee traf sie in aller Kälte, während sich
die Strahlen einen Weg durch die Hochhäuser zu ihr erschlichen. Seit
ihrer Jugend kannte Eliza nur diesen faden Sonnenuntergang. Man hatte vor
Jahren eine Solarstation in den Orbit geschossen und damit zwei Probleme
gelöst: Energieknappheit und die Reduzierung des Sonnenlichts, um die
Atmosphäre nicht weiter aufzuheizen. Ihr P sagte damals, es gäbe da einen
Zusammenhang, sie war jedoch jung und konnte ihm nicht ganz folgen.

Frank schaute auf sein Tele-Armband und riss seine M am Arm zu sich: ,,Da ist
eine Wohn frei, keine 200 weit von hier!'' Eliza, von der Prozedur des
Doktorix sichtbar erschöpft, nickte ihm zu. ,,Dann muss sich dein P heute
eine Andere suche.'' Frank verzog daraufhin sein Gesicht, als wenn er etwas
gefragt wurde, worauf er nicht antworten wollte: ,,Du, Mama, der P hat schon
eine Andere.'' Damit meinte er nicht nur eine Wohnung. Das war nicht unbedingt
dass, was sie jetzt hören wollte.
Schweigend gingen beide Richtung Strandimitat. Sand gab es schon lange nicht
mehr, da er für Wohngebäude und Straßen verbaut wurde. Der Strand bestand
heute aus feinem Kunststoff Granulat. Eliza erinnerte sich an alte Bilder
von Sandstränden vor der Zeit der Baustoffknappheit. Sie verstand nicht,
wieso man auch Straßen in dieser enormen Fülle aus dem Sand gebaut hatte,
die nun einsam und von Birken überwuchert auf Trassen das Umland
zerschneiden.

,,Da hinten ist es!'', rief Frank und rannte voraus zum Eingang des
Wohnkomplexes, ,,Ist das nicht Wahn! Wir haben ein Wohn 
direkt am Immi!'' ,,Frank!'', ermahnte Eliza ihr K, ,,kürze doch nicht immer alles ab. Es
heißt Strandimitat.'' Frank drehte sich mit Stirnrunzeln zu ihr: ,,M,
was ist denn ein Strand?''

## Mond

Das System machte bei der Replizierung der persönlichen Gegenstände aus
der Cloud manchmal Fehler. Mal wurden Pflanzen als Nahrung klassifiziert
und zubereitet, mal geliefert in tief gefrorenen Zustand und mal neu
gekauft und das Original als Bio-Substanz recycelt. Aus diesem Grund waren
nur synthetische Haustiere empfohlen. Eliza und Frank hatten mal eine
Katze, die leider als Heizkissen repliziert wurde.

Eliza ließ sich in das Sofa im Gang vor Ihrer neuen Wohn fallen. Frank
ging in den Übergangsinfo-Raum. Bis zur Fertigstellung der Wohn
konnte er die Recherche für die Hausaufgaben dort an einem
Projektor-Terminal starten. Während Eliza wie hypnotisiert auf den leicht
flackernden Fortschrittsbalken der Wohnungstüre starrte, stieß Frank
auf eine Werbung, die ihn bei der Recherche total ablenkte:

>   Jetzt neu im Wohnkomplex zur leuchtenden Qualle: Sleep-in-Soap, dass
>   Waschmittel zum schnellen Einschlafen! Dank unserer chemisch
>   markierten Wäsche können Sie nun Ihr Bett täglich mit Sleep-in-Soap
>   waschen lassen! Eine herkömmliche Spritze entfällt!
>   Sleep-in-Soap: Schlafen, als wäre es das natürlichste auf der Welt!

Frank lief zu seiner M zurück, die gerade die durchsichtige Versiegelungsfolie
an der Türe entfernt. ,,Schlafen als sei es wieder natürlich, M!''

Eliza: ,,Wie bitte? Was meinst du Frank?''

Frank: ,,Die haben hier Sleepsoap in der Wäsche!''

Eliza: ,,Was ist\ldots''

Frank: ,,Wir müssen uns zum Schlafen keine mehr in den Kopf hauen, die machen
das hier mit dem Waschmittel.''

Eliza: ,,Wir können uns sowas Modernes nicht leisten.''

Frank sackte enttäuscht zusammen. Seit der Sprengung des Mondes war er es leid,
Abends die Spritze hinters Ohr zu setzen, um schlafen zu können. Man hatte
bei der Sprengung nicht bedacht, wie viele Lebewesen auf die Licht- und
Gezeitenwirkung angewiesen sein könnten -- Menschen inklusive. Alle mussten von
der Krankenversorgung spätestens nach 3 Tagen mit einer Sleep-in-Vitro
Spritze einen Schlafzyklus starten. Sleep-in-Vivo, also der Schlaf durch
den natürlichen Prozess, war für alle Säugetiere nicht mehr möglich. Jetzt,
nach zwei Jahren, war man immer noch damit beschäftigt, die Folgen auf die
Umwelt ein zu dämmen. Zum Glück musste das Elizas und Franks Katze nicht
durchmachen. Es war eine weltweite Krise, bei der viele starben oder
dem Wahnsinn verfielen. Den Grund für den Zusammenhang zwischen Schlaf und
Mond hatte man bis heute noch nicht herausgefunden; ebenso wenig den Grund,
warum die Firma damals den Mond gesprengt hatte. Man arbeitete bereits
an anderen, effizienteren Konzepten ohne künstliche Hormone. Das
vielversprechenste war Sleep-in-Silico, wo man eine Traumsimulation
durchleben konnte. Es war ein gelenkter Traum bei *vollem* Bewusstsein
mit der selben Erholungswirkung. Man war sich nur noch nicht sicher,
welche Teile des Bewusstseins zur Erholung dennoch abzuschalten sind. Dies 
wollte man mit unterschwellige Botschaften in der simulierten Umgebung
erreichen.

Eliza: ,,Weißt du was, Frank? Vergiss deine Hausaufgaben. Ich bin so
müde. Lass uns doch jetzt schon eine hinters Ohr knallen und schlafen.''

Frank: ,,Aber... die Schule! Nach der Schlappe mit meinen mutierten
Roboterameisen muss ich...''

Eliza: ,,Gar nichts musst du. Ich melde dich morgen beim Bildungswesen
als Nachzügler an, damit du was wirst.''

Frank: ,,Oh ja M! Das wäre großartig!''

Eliza war etwas überrascht auf die Reaktion, hatte sie doch ihrem K
genau erklärt, warum sie das staatliche Bildungswesen in der Art so
ablehnte. Eigentlich war sie sogar über sich selbst überrascht. Vermutlich
hätte sich Frank nicht im Übergangsinfo-Raum an den Terminal stellen
dürfen. Die Werbung dort war besonders -- sie kam nicht auf
das Wort -- war besonders ungut.

## Mehr oder Weniger

Doctorix Schneider hatte es endlich in der Hand: den Bescheid Tests ohne Clone
machen zu können. Seine Kollegen finden diesen Schritt unnötig. Aufgrund der
Psyche von Gefangenen werden sie bei Versuchen am lebenden menschlichen Gehirn
weiterhin Medi-Clone verwenden. Man hatte für diverse Zwecke eine
Sorte Mensch geschaffen, welche sehr leicht mit Licht, welches mit Lichtleiterkabel
in das Nervengewebe geleitet wird, narkotisiert werden können. Das Schmerzempfinden
war ausserdem normiert: Sollte ein Medi-Clon sein unbehagen durch Zittern
oder animalische Klagelaute äußern, so tun dies alle ab dem selben Schmerzniveau.
Sollte es zu einem Gewöhnungseffekt kommen, wird dies durch kleine Implantate
ausgeglichen und der Schmerz intern verstärkt. Schneider braucht jedoch mehr,
als einen Kultur- und Wissen- ungeprägten Medi-Clon. Aufgrund alter Forschungsergebnisse
wusste er, in wie weit das Gehirn bei Schaden andere Areale die gestörten
und geschädigten Bereiche ersetzen können. Er fragt sich, was die Minimalgröße
wäre und was passiert, wenn man nach einer ausgleichenden Übernahme einer
Funktion im Gehirn, die zuvor geschädigten, entfernten oder nur betäubten Bereiche
wieder aktiviert. Schneider geht davon aus, das deren Funktion neu belegt
wird oder die alte Funktion dieser Gehirnregion besser als vorher wird.
Mit ,,kompletten Hohlbirnen'' könne er dies nun mal nicht gut testen.

Ein paar Tage später kommt seine erste Lieferung. Die in kryostase eingefrorenen
Gefangenen hatten nur in einer kurzen Tauphase ein kleines Resozialisierungsprogramm
durchlaufen, so dass sie freiwillig diesen Tests an sich zustimmten. Man wolle
da vom Justizverwahrer auf der sicheren Seite sein. Normalerweise ist dieses
Programm deutlich länger und komplexer, aber Schneider drängte auf eine schnelle
Auslieferung und so war nur dieser freiwillige Aspekt eingepflegt worden. ...

## Zu neuen Welten

Mitten in der Nacht machte die Heizungsanlage seltsame Geräusche. Er erinnerte
sich an alte Filme, in denen diese Töne ein futuristisches Antriebssystem
darstellen sollten. ...

## Abrissbirne

Man hatte beschlossen, die Erde zu wechseln. Daher wurde ein Dekret erlassen,
welches explizit Eingriffe in die Umwelt als nicht strafbar ansah. ...
