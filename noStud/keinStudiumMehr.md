Interpretation von ,,Keine Poesie mehr'' von Otto Sommerstorff


    Ich wandelte stumm im Gange
    so für mich hin,
    und ein gar lieber Gedanke
    lag mir im Sinn:

    Wenn's hier ein Labor gäbe
    mit Hardware fein,
    ich zögerte wohl nicht lange
    und ging' hinein.

    Und hätt' es viele Geräte,
    wie schön wär' das!
    Und mit ihnen lernen und testen,
    das wär' ein Spaß!

    Ich bliebe da und vergäße
    wohl all mein Leid
    beim kreativen Basteln
    und in Gedanken ganz weit ...

    Das Labor und die Freiheit schon
    vergessen! beim Blick auf die Uhr.
    Nichts davon an der Hochschule
    als nur Testate und Klausur.

    Ich wandelte stumm nach Hause,
    ich weiß nicht, nee -
    Verschwunden ist durch die Lehre
    meine eigene Idee.

  