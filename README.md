# Unsere Welt und wie es so... läuft (und das Leben da drinnen)

Zusammenstellung einiger Texte von mir.

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Namensnennung - Nicht kommerziell - Keine Bearbeitungen 4.0 International Lizenz</a>.

Kommerzielle Nutzung nur unter vorbehalt. Vorlesen und Aufzeichen stellen
keine Bearbeitung dar.

## Inhalt

- [Evolution](Evolution/Evolution.md) oder [PDF](Evolution/Evolution.pdf)
- [Kein Studium mehr](noStud/keinStudiumMehr.md) oder [PDF](noStud/keinStudiumMehr.pdf)
- [Zuende](zuEnde/Zuende.md) oder [PDF](zuEnde/Zuende.pdf)
- [Haltestelle Belsenplatz](HaltestelleBelsenplatz/HaltestelleBelsenplatz.md) oder [PDF](HaltestelleBelsenplatz/HaltestelleBelsenplatz.pdf)
- [Die Zeit vorm ersten Kaffee](vorKaffee/DieZeitVormErstenKaffee.md) oder [PDF](vorKaffee/DieZeitVormErstenKaffee.pdf)
- [System Green ist Menschenfleisch](SystemGreen/README.md) oder [PDF](SystemGreen/SystemGreen.pdf)
- [sternewach](Sternewach/sternewach.md) oder [PDF](Sternewach/sternewach.pdf)
- [Immerwährende Verantwortung](Verantwortung/README.md) oder [PDF](Verantwortung/Verantwortung.pdf)
- [Von Kohl, Krähen und Gift](KraehenUndGift/KraehenUndGift.md) oder [PDF](KraehenUndGift/KraehenUndGift.pdf)

