\begin{center}
\begin{Large}Immerwährende Verantwortung\end{Large} \\
Die Everlasting Privacy und die Ethik der Zukunft \\
\emph{von Jochen Peters (Juni 2019)}
\end{center}

## 

Ein ca 5x5m Kellerraum, 2.30m hoch. weiss gekachelt. 1 Tisch, 2 Stühle,
gut ausgeleuchtet. Ein kleiner Mann mittleren Alters sitzt am Tisch. er hat
ein art latzhose in weiss an. Schuhe ähneln einmal-pantoffeln, wie sie
in museen zum schutz von parkettböde in mode waren. Auf der Latzhose ist vorne
ein großes grünes "B" in form eines Blatts abgebildet. darunter sind
in schwarz die Buchstaben und Zahlen "Noxville 2081" mit einer Schablone
gesprayt worden.

Eine 2. Person mit blauem anzug und einer Mappe in der hand kommt rein.
Er hat eine Plakette am Anzug mit der Aufschrift: "Beispielverhör 5011".


5011: "So, wen haben wir denn da."

Noxville: "Warum hat man mich von meiner Arbeit entfernt?"

5011: "Sie sollten es wissen, schließlich arbeiten sie in der Fabrik für Informationswahrheit."

Noxville: "Hat es mit den Wahlen zwischen 2022 und 2094 zu tun?"

5011: "Natürlich."

Noxville schluckt. Er nuschelt: "Naive, dumme Menschen damals."

5011: "Es ist eine Ironie, dass sie beim Aufbereiten der Informationen der Vergangenheit
nun diese Sicherheitslücke in den elektonischen Wahlen gefunden habe."

Noxville: "Ironie?"

5011: "Verzeihung, ich habe mich falsch ausgedrückt. Wir müssen Sie leider Lagern wegen des Funds."

Noxville: "Ich verstehe immer noch nicht ganz..."

5011: "Wir können Sie dort nicht weiter beschäftigen, da wir Ihnen durch die
Wahl ihrer Vorfahren das Vertrauen entziehen müssen."

Noxville: "Haben Sie bereits die Wahlen von damals komplett entschlüsselt?"

5011: "Natürlich."

Noxville: "Gibt es bei meinen Vorfahren Auffälligkeiten?"

5011: "Natürlich."

ein paar sekunden stille

5011: "Schauen Sie, seit Jahrzehnten geben wir auf die Umwelt acht und es bringt nichts mehr.
Der Genpool der Tiere und Insekten schrumpf weiter und damals waren Menschen wie ihrer Vorfahren
durch ihr Handeln und die politische Wahl dafür verantwortlich."

Noxville: "Das Generationen Umwelt Verantwortungsgesetz ist doch erst sehr neu!"

5011: "Sie sind zusammen mit Ihrer Entdeckung nun der erste, den es trifft."

Noxville: "Meine Vorfahren kannten das Gesetz von heute nicht. Wie kann ich für deren Fehler verantwortlich gemacht werden?"

5011: "Oh, Noxville. Gut, dass wir sie erwischt haben. Sie wenden die selbe Ethik der Menschen von damals an.
Es ist nur ein Zeichen dafür, wie richtig die generationenübergreifende Verantwortung ist. SIE sind für deren
Fehler deswegen verantwortlich, weil SIE durch deren Vererbung und Sozalisation die selben Straftaten machen würden!"

Noxville schluckt erneut.

Noxville: "Aber..."

5011: "nichts Aber! Mir platzt gleich der Kragen! Weisst du, wie viele ich von euch noch abfertigen muss!? Deine
Entdeckung hat hunderttausende Umweltterroristen aufgedeckt, dessen Vorfahren damals die politisch falschen Parteien
gewählt haben! Und mit dir, du Wüstchen, fange ich an!"

Der Mann im Anzug schlägt die Unterlagen heftig auf den Tisch, worauf dieser langsam
mit dem Geräusch einer großen Baumaschine in den Boden fährt. Der Mann dreht sich
auf dem Absatz um und verlässt den Raum. Noxville versucht seine "Latzhose" zu
verreißen. Jetzt wird erst klar, dass die Latzhose mit ihm am Stuhl befestigt
ist! Ein zerreißen gelingt ihm nicht. Die Tischplatte ist nun im Boden versunken
und ein Loch ist im Boden entstanden. Noxville schreit während sein Stuhl langsam
mit ihm in die Grube klappt. Eine Schiebevorrichtung verschießt das Loch.
Man hört einen dumpfen Schlag. Noxville ist still. Das Licht geht aus und
ein blaues, schwaches Notlicht geht an. In einer Wand geht eine Türe auf und
ein Stuhl mit einer anderen, schlafenden Person darauf fährt in den Raum. Man 
sieht nur Siluetten: die Person scheint, als der Stuhl in die Raummitte 
kommt, langsam aufzuwachen. Nun geht auch das blaue Notlicht aus. 
