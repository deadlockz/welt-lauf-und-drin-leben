Neulich bin ich am Teich vom der Physik-Gebäude vorbeigegangen,
und mir viel auf, dass die Fische in der Hitze und in
dem sehr flachen Gewässer mehr Luft an der Wasseroberfläche
schnappen, als unter Wasser. Also sind eher über- als unterwasser. Wenn der Teich
warm wird, geht der Sauerstoffgehalt im Wasser stark
zurück. Da kam mir eine tolle Idee, wie das mit der
Evolution gewesen sein koennte:


Zuerst war der Physiker.

Er entwarf eine Zeitmaschine und reiste vor Gottes erscheinen in
die Vergangenheit. Der Physiker sagte: ,,Macht mir
hier in der prallen Sonne, einen zu flachen Teich hin.''

Er sah, dass es dumm war. Darum sagte er: ,,Das ist dumm. Das Wasser
ist zu still. Macht mir da sehr viele Fische rein, so dass es plätschert!''

Er sah, dass die Fische mehr über- als unterwasser ahtmen mussten. Das war ihm egal.

Gott sah nun zum ersten Mal auf die Erde, und hatte bei so viel Dummheit keine
Lust, sich einzumischen. Er ging einen Trinken, und ließ den Physiker sterben, weil
er zu dieser Zeit niemanden haben wollte, der ihm ins Handwerk pfuscht.

Mit der Zeit lehrnten die Fische an Land zu gehen. Es entstanden
Lebewesen, die sich Menschen nannten. Sie glaubten, sich weiter zu
entwickeln und wurden Naturwissenschaftler. Gott verzieh nun dem
Physiker, der das mit dem Teich fabriziert hatte. Nun ließ er den
Physiker wieder entstehen. Danach hat er sich georfeigt, denn er
sah, dass auch das dumm war.
